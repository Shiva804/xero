const db = require("../../models");

async function addClient(data) {
  try {
    const dbdata = await db.client.create({
      client_id: data.clientID,
      client_name: data.clientName,
    });
    return dbdata;
  } catch {
    return 500;
  }
}

async function findClient(clientID) {
  try {
    const dbdata = await db.client.findAll({
      where: { client_id: clientID },
    });
    if (Object.keys(dbdata).length !== 0 && dbdata.constructor !== Object) {
      return dbdata;
    } else {
      return 400;
    }
  } catch {
    return 500;
  }
}

async function deleteClient(clientID) {
  try {
    const dbdata = await db.client.destroy({
      where: { client_id: clientID },
    });

    return 200;
  } catch {
    return 500;
  }
}

module.exports = { addClient, findClient, deleteClient };
