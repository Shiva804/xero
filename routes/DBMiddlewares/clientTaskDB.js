const db = require("../../models");

async function findClientTask(clientID) {
  try {
    const dbdata = await db.client_task.findAll({
      where: {
        fk_client_id: clientID,
      },
      include: [db.client, db.task_type],
    });

    if (Object.keys(dbdata).length !== 0 && dbdata.constructor !== Object) {
      return dbdata;
    }
    return 400;
  } catch {
    return 500;
  }
}

async function addClientTask(data) {
  try {
    const dbdata = await db.client_task.create({
      fk_client_id: data.clientID,
      fk_task_id: data.taskID,
    });
    return dbdata;
  } catch {
    return 500;
  }
}
async function deleteClientTask(clientID) {
  try {
    const dbdata = await db.client_task.destroy({
      where: {
        fk_client_id: clientID,
      },
    });
    return 200;
  } catch {
    return 500;
  }
}
module.exports = { addClientTask, findClientTask, deleteClientTask };
