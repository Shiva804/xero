module.exports = (sequelize, DataTypes) => {
  const task_vendor = sequelize.define("task_vendor", {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
  });

  return task_vendor;
};
