const db = require("../../models");

async function addTaskType(data) {
  try {
    const dbdata = await db.task_type.create({
      task_id: data.taskID,
      task_name: data.taskName,
    });

    return dbdata;
  } catch {
    return 500;
  }
}

async function findTaskType(taskID) {
  try {
    console.log(taskID);

    const dbdata = await db.task_type.findAll({
      where: { task_id: taskID },
    });
    console.log(dbdata);

    if (Object.keys(dbdata).length !== 0 && dbdata.constructor !== Object) {
      return dbdata;
    } else {
      return 400;
    }
  } catch {
    return 500;
  }
}

async function deleteTaskType(taskID) {
  try {
    const dbdata = await db.task_type.destroy({
      where: {
        task_id: taskID,
      },
    });

    return 200;
  } catch {
    return 500;
  }
}

module.exports = { findTaskType, addTaskType, deleteTaskType };
