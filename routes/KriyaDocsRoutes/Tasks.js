const { xero } = require("../../xeroInstance");

const { PurchaseOrders } = require("xero-node");
const moment = require("moment");

const vendorTask = require("../DBMiddlewares/vendorTaskDB");
const taskType = require("../DBMiddlewares/taskTypeDB");
const po = require("../DBMiddlewares/vendorPosDB");
const task = require("../DBMiddlewares/tasksDB");

function route(fastify, options, next) {
  fastify.post("/tasks", async (req, res) => {
    const vendorData = await vendorTask.findVendorTask(req.body.taskTypeID);

    if (vendorData == 400) res.status(400).send("Check parameter");
    if (vendorData == 500) res.status(500).send("Error");

    //If the task belong to the vendor then authorize
    if (vendorData[0].vendor.vendor_id == req.body.userID) {
      const vendor_type = vendorData[0].vendor.vendor_type;
      const vendor_name = vendorData[0].vendor.vendor_name;

      //Internal employee
      if (vendor_type == "Internal") {
        //Create a time entry for the internal vendor
        const timeEntry = {
          userId: req.body.userID,
          taskId: req.body.taskTypeID,
          dateUtc: new Date(),
          duration: req.body.uomValue,
        };

        const data = await xero.projectApi.createTimeEntry(
          req.session.activeTenant.tenantId,
          req.body.projectID,
          timeEntry
        );

        //Add the data to the Database
        const dbTask = {
          status: req.body.status,
          uom: req.body.uom,
          uom_value: req.body.uomValue,
          manuscript_id: req.body.manuscriptID,
          fk_task_id: req.body.taskTypeID,
          fk_vendor_id: req.body.userID,
          fk_project_id: req.body.projectID,
        };

        const TaskDb = await task.addTask(dbTask);

        if (TaskDb == 500) res.status(500).send("Error");

        res.status(200).send(TaskDb);
      }

      if (vendor_type == "External") {
        //Find the task name with the taskTypeID
        const response_task_name = await taskType.findTaskType(
          req.body.taskTypeID
        );
        if (response_task_name == 500) res.status(500).send("Error");
        if (response_task_name == 400) res.status(500).send("Check Parameters");

        const task_name = await response_task_name[0].task_name;

        //Fetch the contact ID's from xero

        const contact = await xero.accountingApi.getContacts(
          req.session.activeTenant.tenantId
        );

        //Function to filter the contact ID of the vendor
        async function fetchContactID(contact, vendor_name) {
          for (i in contact.body.contacts) {
            if (contact.body.contacts[i].name == vendor_name)
              return contact.body.contacts[i].contactID;
          }
        }

        const ContactID = await fetchContactID(contact, vendor_name);

        //Fetching the unit amount of the task
        const item = await xero.accountingApi.getItem(
          req.session.activeTenant.tenantId,
          req.body.taskTypeID
        );
        console.log(item);
        const unitAmount = item.body.items[0].salesDetails.unitPrice;
        console.log(unitAmount);
        const date = moment().format("YYYY-MM-DD");
        const delieveryDate = moment().add(10, "days").format("YYYY-MM-DD");

        //Create a purchase order

        const newPurchaseOrder = {
          contact: {
            contactID: ContactID,
          },
          date: date,
          deliveryDate: delieveryDate,
          reference: req.body.manuscriptID,
          lineItems: [
            {
              description: task_name,
              quantity: req.body.uomValue,
              unitAmount: unitAmount,
            },
          ],

          currencyCode: "INR",
        };

        const purchaseOrders = new PurchaseOrders();
        purchaseOrders.purchaseOrders = [newPurchaseOrder];

        const data = await xero.accountingApi.createPurchaseOrders(
          req.session.activeTenant.tenantId,
          purchaseOrders
        );

        //Add the data to the database
        const dbPO = {
          poID: data.body.purchaseOrders[0].purchaseOrderID,
          vendorID: req.body.userID,
        };

        const poDb = await po.addVendorPO(dbPO);

        if (poDb == 500) res.status(500).send("Error");

        res.status(200).send(poDb);
      }
    } else {
      res.status(401).send("Unautherized");
    }
  });

  next();
}

module.exports = route;
