module.exports = (sequelize, DataTypes) => {
  const client_task = sequelize.define("client_task", {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
  });

  return client_task;
};
