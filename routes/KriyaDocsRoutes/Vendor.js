const vendorTask = require("../DBMiddlewares/vendorTaskDB");
const clientTask = require("../DBMiddlewares/clientTaskDB");

function route(fastify, options, next) {
  fastify.get("/vendors/:id", async (req, res) => {
    const key = Object.keys(req.query);

    if (key[0] == "taskTypeID") {
      const vendor = await vendorTask.findVendorTask(req.query.taskTypeID);

      if (vendor == 400) res.status(400).send("Check parameter");

      if (vendor == 500) res.status(500).send("Error");
      console.log(vendor[0].vendor.vendor_id);
      res.status(200).send(vendor);
    }

    if (key[0] == "clientID") {
      //Find the tasks of the client
      const client = await clientTask.findClientTask(req.query.clientID);

      if (client == 400) res.status(400).send("Check parameter");
      if (client == 500) res.status(500).send("Error");
      vendors = [];

      //Find Vendors of the tasks of the client
      for (const i in client) {
        const taskID = client[i].dataValues.fk_task_id;
        const vendor = await vendorTask.findVendorTask(taskID);

        if (vendor == 400) res.status(400).send("Check parameter");
        if (vendor == 500) res.status(500).send("Error");

        vendors.push(vendor[0]);
      }

      res.status(200).send(vendors);
    }
  });

  next();
}

module.exports = route;
