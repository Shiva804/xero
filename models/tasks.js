module.exports = (sequelize, DataTypes) => {
  const task = sequelize.define("task", {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    uom: {
      type: DataTypes.STRING(45),
    },
    uom_value: {
      type: DataTypes.INTEGER,
    },
    manuscript_id: {
      type: DataTypes.STRING(45),
    },
    status: {
      type: DataTypes.STRING(100),
    },
    sales_invoice_id: {
      type: DataTypes.STRING(45),
    },

    score: {
      type: DataTypes.INTEGER,
    },
  });

  return task;
};
