module.exports = (sequelize, DataTypes) => {
  const client = sequelize.define("client", {
    client_id: {
      type: DataTypes.STRING(45),
      primaryKey: true,
    },
    client_name: {
      type: DataTypes.STRING(200),
      allowNull: false,
    },
  });

  return client;
};
