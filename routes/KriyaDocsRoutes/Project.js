const { xero } = require("../../xeroInstance");
const project = require("../DBMiddlewares/projectDB");

function route(fastify, options, next) {
  fastify.post("/projects", async (req, res) => {
    try {
      if (req.body.clientID && req.body.projectName) {
        //New Project
        const Newproject = {
          contactId: req.body.clientID,
          name: req.body.projectName,
        };

        const data = await xero.projectApi.createProject(
          req.session.activeTenant.tenantId,
          Newproject
        );
        //Add project to the DB
        dbProject = {
          projectID: data.body.projectId,
          clientID: data.body.contactId,
          status: data.body.status,
        };

        const projectDb = await project.addProject(dbData);

        if (projectDb == 400) res.status(400).send("Check parameter");
        if (projectDb == 500) res.status(500).send("Error");

        res.status(200).send(projectDb);
      } else {
        res.status(400).send("Check paramater name!");
      }
    } catch {
      res.status(500).send("Error");
    }
  });

  next();
}

module.exports = route;
