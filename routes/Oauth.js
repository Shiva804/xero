
const jwtDecode = require('jwt-decode');
const {xero} = require("../xeroInstance");


function route(fastify,options,next) {




    //A function to just console log the auth data
    const authenticationData = (req,res) => 
    {
        return {
            decodedIdToken: req.session.decodedIdToken,
            decodedAccessToken: req.session.decodedAccessToken,
            tokenSet: req.session.tokenSet,
            allTenants: req.session.allTenants,
            activeTenant: req.session.activeTenant,
        };
    };
    
    
    //connect to xero oauth
    fastify.get('/connect', async (req,res) =>
     {
        try {
            const consentUrl= await xero.buildConsentUrl();
            res.redirect(consentUrl);
        } catch (err) {
            res.send(err);
        }
    });
    
    
    
    
    //callback function which further redirect to Organisation route
    fastify.get('/callback', async (req, res) =>
     {
        try {
            const tokenSet = await xero.apiCallback(req.url);
            await xero.updateTenants();
    
            const decodedIdToken = jwtDecode(tokenSet.id_token);
            const decodedAccessToken= jwtDecode(tokenSet.access_token);
    
            req.session.decodedIdToken = decodedIdToken;
            req.session.decodedAccessToken = decodedAccessToken;
            req.session.tokenSet = tokenSet;
            req.session.allTenants = xero.tenants;
            req.session.activeTenant = xero.tenants[0];
    
            const authData = authenticationData(req, res);
            console.log(authData)
            res.redirect('/organisation');


        } catch (err) {
            res.send(err);
        }
    });

    

    



next()

}




module.exports = route;