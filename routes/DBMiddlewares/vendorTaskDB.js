const db = require("../../models");

async function findVendorTask(taskID) {
  try {
    const dbdata = await db.task_vendor.findAll({
      where: {
        fk_task_id: taskID,
      },
      include: [db.vendor, db.task_type],
    });

    if (Object.keys(dbdata).length !== 0 && dbdata.constructor !== Object) {
      return dbdata;
    } else {
      return 400;
    }
  } catch {
    return 500;
  }
}

async function addVendorTask(data) {
  try {
    const dbdata = await db.task_vendor.create({
      fk_vendor_id: data.vendorID,
      fk_task_id: data.taskID,
    });

    return dbdata;
  } catch {
    return 500;
  }
}

async function deleteVendorTask(taskID) {
  try {
    const dbdata = await db.task_vendor.destroy({
      where: {
        fk_task_id: taskID,
      },
    });

    return 200;
  } catch {
    return 500;
  }
}

module.exports = { findVendorTask, addVendorTask, deleteVendorTask };
