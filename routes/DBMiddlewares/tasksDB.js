const db = require("../../models");

async function addTask(data) {
  try {
    const dbdata = await db.task.create({
      task_id: data.taskID,
      task_description: data.taskDescription,
      status: data.status,
      score: data.score,
      uom: data.uom,
      uom_value: data.uomValue,
      manuscript_id: data.manuscriptID,
      fk_invoice_id: data.invoiceID,
      fk_vendor_id: data.vendorID,
      fk_po_id: data.poID,
      fk_project_id: data.projectID,
    });

    return dbdata;
  } catch {
    return 500;
  }
}

async function findTask(taskID) {
  try {
    const dbdata = await db.task.findAll({
      where: {
        tasK_id: taskID,
      },
    });

    if (dbdata.task_id == taskID) {
      return dbdata;
    } else {
      return 400;
    }
  } catch {
    return 500;
  }
}

async function updateTask(data) {
  try {
    const dbdata = await db.task.update(
      {
        task_description: data.taskDescription,
        status: data.status,
        invoice_id: data.invoiceID,
        po_id: data.poID,
        score: data.score,
      },
      { where: { task_id: data.taskID } }
    );
  } catch {
    return 500;
  }
}

async function deleteTask(taskID) {
  try {
    const dbdata = await db.task.destroy({
      where: {
        tasK_id: taskID,
      },
    });

    return 200;
  } catch {
    return 500;
  }
}

module.exports = { addTask, findTask, updateTask, deleteTask };
