const db = require("../../models");

async function addVendorPO(data) {
  try {
    const dbdata = await db.vendor_po.create({
      po_id: data.poID,
      fk_invoice_id: data.invoiceID,
      fk_vendor_id: data.vendorID,
    });

    return dbdata;
  } catch {
    return 500;
  }
}

async function findVenodrPO(poID) {
  try {
    const dbdata = await db.vendor_po.findAll({
      where: {
        po_id: poID,
      },
    });

    if (dbdata.po_id == poID) {
      return dbdata;
    } else {
      return 400;
    }
  } catch {
    return 500;
  }
}

async function deleteVendorPO(poID) {
  try {
    const dbdata = await db.vendor_po.destroy({
      where: {
        po_id: poID,
      },
    });

    return 200;
  } catch {
    return 500;
  }
}

module.exports = { addVendorPO, findVenodrPO, deleteVendorPO };
