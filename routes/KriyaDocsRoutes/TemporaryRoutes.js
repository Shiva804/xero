//Temporary Routes for Testing

const { xero } = require("../../xeroInstance");
const vendorTask = require("../DBMiddlewares/vendorTaskDB");

const { Items } = require("xero-node");
const moment = require("moment");

function route(fastify, options, next) {
  //get items
  fastify.get("/items/:id", async (req, res) => {
    try {
      const tokenSet = await xero.readTokenSet();
      const id = req.params.id;
      const data = await xero.accountingApi.getItem(
        req.session.activeTenant.tenantId,
        id
      );

      const unitAmount = data.body.items[0].salesDetails.unitPrice;

      res.send(unitAmount);
    } catch (err) {
      res.send(err);
    }
  });
  ///post items
  fastify.post("/items", async (req, res) => {
    try {
      const Newitem = {
        code: req.body.code,
        name: req.body.name,
        purchaseDetails: {},
        salesDetails: {
          unitPrice: req.body.unitPrice,
          taxType: "NONE",
          accountCode: "200",
        },
      };

      const item = new Items();
      item.items = [Newitem];

      const data = await xero.accountingApi.createItems(
        req.session.activeTenant.tenantId,
        item
      );

      res.send(data.body);
    } catch (err) {
      res.send(err);
    }
  });

  ///Checking contacts
  fastify.get("/contact/:id", async (req, res) => {
    const contact = await xero.accountingApi.getContacts(
      req.session.activeTenant.tenantId
    );
    console.log(contact);
    console.log(contact.length);
    console.log(contact[0]);
    for (i in contact.body.contacts) {
      if (contact.body.contacts[i].name == req.params.id)
        res.send(contact.body.contacts[i].contactID);
    }
    // const co = contact.body.contacts[0].name;

    // res.send(co);

    // const date = moment().format("YYYY-MM-DD");
    // const delieveryDate = moment().add(10, "days").format("YYYY-MM-DD");
    // res.send({ date, delieveryDate });
  });

  next();
}

module.exports = route;
