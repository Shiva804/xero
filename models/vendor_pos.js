module.exports = (sequelize, DataTypes) => {
  const vendor_po = sequelize.define("vendor_po", {
    po_id: {
      type: DataTypes.STRING(45),
      allowNull: false,
      primaryKey: true,
    },
  });

  return vendor_po;
};
