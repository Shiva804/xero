module.exports = (sequelize, DataTypes) => {
  const project = sequelize.define("project", {
    project_id: {
      type: DataTypes.STRING(45),
      allowNull: false,
      primaryKey: true,
    },
    client_id: {
      type: DataTypes.STRING(45),
      allowNull: false,
    },
    status: {
      type: DataTypes.STRING(100),
      allowNull: false,
    },
    invoice_id: {
      type: DataTypes.STRING(45),
    },
    job_id: {
      type: DataTypes.STRING(100),
    },
  });
  return project;
};
