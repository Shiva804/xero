module.exports = (sequelize, DataTypes) => {
  const vendor = sequelize.define("vendor", {
    vendor_id: {
      type: DataTypes.STRING(45),
      allowNull: false,
      primaryKey: true,
    },
    vendor_name: {
      type: DataTypes.STRING(200),
      allowNull: false,
    },
    vendor_type: {
      type: DataTypes.STRING(200),
      allowNull: false,
    },
    daily_limit: {
      type: DataTypes.INTEGER,
    },
    limit_unit: {
      type: DataTypes.STRING(200),
    },
    score: {
      type: DataTypes.INTEGER,
    },
  });
  return vendor;
};
