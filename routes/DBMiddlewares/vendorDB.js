const db = require("../../models");

async function findVendor(vendorID) {
  try {
    const dbdata = await db.vendor.findAll({
      where: { vendor_id: vendorID },
    });
    if (Object.keys(dbdata).length !== 0 && dbdata.constructor !== Object) {
      return dbdata;
    } else {
      return 400;
    }
  } catch {
    return 500;
  }
}

async function addVendor(data) {
  try {
    const dbdata = await db.vendor.create({
      vendor_id: data.vendorID,
      vendor_name: data.vendorName,
      vendor_type: data.vendorType,
      daily_limit: data.dailyLimit,
      limit_unit: data.limitUnit,
      score: data.score,
    });

    return dbdata;
  } catch {
    return 500;
  }
}
async function updateVendor(data) {
  try {
    const dbdata = await db.vendor.update(
      {
        vendor_name: data.vendorName,
        vendor_type: data.vendorType,
        daily_limit: data.dailyLimit,
        limit_unit: data.limitUnit,
        score: data.score,
      },
      { where: { vendor_id: data.vendorID } }
    );

    return dbdata;
  } catch {
    return 500;
  }
}

async function deleteVendor(vendorID) {
  try {
    const dbdata = await db.vendor.destroy({
      where: { vendor_id: vendorID },
    });

    return 200;
  } catch {
    return 500;
  }
}
module.exports = { addVendor, updateVendor, deleteVendor, findVendor };
