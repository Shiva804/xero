const db = require("../../models");

async function addProject(data) {
  try {
    const dbdata = await db.project.create({
      project_id: data.projectID,
      client_id: data.clientID,
      status: data.status,
      invoice_id: data.invoiceID,
      job_id: data.jobID,
    });
    return dbdata;
  } catch {
    return 500;
  }
}
async function findProject(projectID) {
  try {
    const dbdata = await db.project.findAll({
      where: { project_id: projectID },
    });

    if (Object.keys(dbdata).length !== 0 && dbdata.constructor !== Object) {
      return dbdata;
    } else {
      return 400;
    }
  } catch {
    return 500;
  }
}

async function updateProject(projectID) {
  try {
    const dbdata = await db.project.update(
      {
        status: data.status,
        invoice_id: data.invoiceID,
        job_id: data.jobID,
      },
      { where: { project_id: projectID } }
    );

    return dbdata;
  } catch {
    return 500;
  }
}

async function deleteProject(projectID) {
  try {
    const dbdata = await db.project.destroy({
      where: { project_id: projectID },
    });

    return "done";
  } catch {
    return 500;
  }
}

module.exports = { addProject, updateProject, findProject, deleteProject };
