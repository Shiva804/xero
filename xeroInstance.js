const xeroNode = require('xero-node');
require("dotenv").config();

const client_id= process.env.CLIENT_ID;
const client_secret = process.env.CLIENT_SECRET;
const redirectUrl = process.env.REDIRECT_URI;
const scopes = 'openid profile email accounting.settings accounting.reports.read accounting.journals.read accounting.contacts accounting.attachments accounting.transactions projects projects.read offline_access';

const xero = new xeroNode.XeroClient({
	
    clientId: client_id,
    clientSecret: client_secret,
    redirectUris: [redirectUrl],
    scopes: scopes.split(' '),
});


module.exports = {xero}