module.exports = (sequelize, DataTypes) => {
  const vendor_invoice = sequelize.define("vendor_invoice", {
    invoice_id: {
      type: DataTypes.STRING(45),
      allowNull: false,
      primaryKey: true,
    },
  });

  return vendor_invoice;
};
