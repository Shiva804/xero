module.exports = (sequelize, DataTypes) => {
  const task_type = sequelize.define("task_type", {
    task_id: {
      type: DataTypes.STRING(45),
      primaryKey: true,
      allowNull: false,
    },
    task_name: {
      type: DataTypes.STRING(200),
      allowNull: false,
    },
  });

  return task_type;
};
