const fastify = require("fastify");
const app = fastify({ logger: true });
const fastifyCookie = require("fastify-cookie");
const session = require("fastify-session");
//require middlewares
const oauth = require("./routes/Oauth");

//Database Routes
const db = require("./models");

//KriyaDocsRoutes
const Project = require("./routes/KriyaDocsRoutes/Project");
const Task = require("./routes/KriyaDocsRoutes/Tasks");
const Vendor = require("./routes/KriyaDocsRoutes/Vendor");
const TemporaryRoute = require("./routes/KriyaDocsRoutes/TemporaryRoutes");

require("dotenv").config();
app.register(fastifyCookie);

//Associations

//Client Task
db.client_task.belongsTo(db.client, { foreignKey: "fk_client_id" });
db.client_task.belongsTo(db.task_type, { foreignKey: "fk_task_id" });

//Vendor Task
db.task_vendor.belongsTo(db.vendor, { foreignKey: "fk_vendor_id" });
db.task_vendor.belongsTo(db.task_type, { foreignKey: "fk_task_id" });

//Vendor Invoices
db.vendor_invoice.belongsTo(db.vendor, { foreignKey: "fk_vendor_id" });

//Vendor POS
db.vendor_po.belongsTo(db.vendor_invoice, { foreignKey: "fk_invoice_id" });
db.vendor_po.belongsTo(db.vendor, { foreignKey: "fk_vendor_id" });

//Tasks
db.task.belongsTo(db.project, { foreignKey: "fk_project_id" });
db.task.belongsTo(db.vendor, { foreignKey: "fk_vendor_id" });
db.task.belongsTo(db.vendor_po, { foreignKey: "fk_po_id" });
db.task.belongsTo(db.task_type, { foreignKey: "fk_task_id" });

db.sequelize.sync().then(() => {
  console.log("Connected to DB successfully");
});

//setting ejs view engine
app.register(require("point-of-view"), {
  engine: {
    ejs: require("ejs"),
  },
});

//session
app.register(session, {
  secret: process.env.SESSION_SECRET,
  resave: false,
  saveUninitialized: true,
  cookie: { secure: false },
});

// Register Middlewares

app.register(oauth);

app.register(require("fastify-cors"), {});
//KriyaDocs
app.register(Project);
app.register(Task);
app.register(Vendor);
app.register(TemporaryRoute);

//main route
app.get("/", async (req, res) => {
  try {
    res.view("./views/app.ejs");
  } catch (err) {
    res.send(err);
  }
});

//organisation route
app.get("/organisation", async (req, res) => {
  res.send("Connected To Organisation!");
});

//start server
const start = async () => {
  try {
    const port = process.env.PORT || 5000;
    await app.listen(port);
    app.log.info(`server listening on ${port}`);
  } catch (err) {
    app.log.error(err);
    process.exit(1);
  }
};

start();
